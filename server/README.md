# Overview

This project serves as the REST API server for the Volunteer Tracking App.

# Getting Started

* Install the JDK from [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install the Postgresql v11.2 from [here](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)
* Install Intell
* Set the password for postgres and use that in the application.properties folder


### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
